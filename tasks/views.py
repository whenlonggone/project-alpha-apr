from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {
        "create_task_form": form,
    }
    return render(request, "task_views/create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.all().filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "task_views/show_my_tasks.html", context)
