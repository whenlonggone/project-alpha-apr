from django.urls import path
from projects.views import (
    show_list_projects,
    show_detail_project,
    create_project,
)

urlpatterns = [
    path("", show_list_projects, name="list_projects"),
    path("<int:id>/", show_detail_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
