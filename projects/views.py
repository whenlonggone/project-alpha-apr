from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


# Create your views here.
@login_required
def show_list_projects(request):
    projects = Project.objects.all().filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "project_views/list_view.html", context)


@login_required
def show_detail_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "project_views/detail_view.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {
        "create_project_form": form,
    }
    return render(request, "project_views/create_project.html", context)
